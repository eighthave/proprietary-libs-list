
Descriptions of proprietary Android libs and patterns and methods for identifying them.

This is mostly a collection of data about proprietary libraries.  Some
copyrighted, proprietary text may be included in this collection under fair use
rules.

# Adding new profiles

* Add a new _.yml_ file under _profiles/_
* Name it after the official name of the product.

# Maintaining profiles

